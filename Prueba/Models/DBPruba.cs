﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class DBPrubas :DbContext
    {
        public DBPrubas() : base("PruebasContext")
        {
            Database.SetInitializer<DBPrubas>(new DBPrubasInitializer());
        }

        public DbSet<PPI_TBL_PLAN_PRUEBAS> PPI_TBL_PLAN_PRUEBAS { get; set; }
        public DbSet<PPI_TBL_LIDER_PDS> PPI_TBL_LIDER_PDS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<PPI_TBL_PLAN_PRUEBAS>().HasRequired(a => a.PPI_TBL_LIDER_PDS_PROYECTO).WithMany().HasForeignKey(a => a.LIDER_PDS_PROYECTO).WillCascadeOnDelete(false);
            modelBuilder.Entity<PPI_TBL_PLAN_PRUEBAS>().HasRequired(a => a.PPI_TBL_LIDER_PDS_RESPONSABLE).WithMany().HasForeignKey(a => a.LIDER_PDS_RESPONSABLE).WillCascadeOnDelete(false);
        }

      
    }
}