﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class DBPrubasInitializer : DropCreateDatabaseIfModelChanges<DBPrubas>
    {
        protected override void Seed(DBPrubas context)
        {

            context.PPI_TBL_LIDER_PDS.Add(new PPI_TBL_LIDER_PDS() { ESTADO = 1, NOMBRE = "Fabián Machuca" });
            context.PPI_TBL_LIDER_PDS.Add(new PPI_TBL_LIDER_PDS() { ESTADO = 1, NOMBRE = " Dayanara Brito" });
            context.PPI_TBL_LIDER_PDS.Add(new PPI_TBL_LIDER_PDS() { ESTADO = 1, NOMBRE = " Otros" });
            context.SaveChanges();
            base.Seed(context);
        }
    }
}