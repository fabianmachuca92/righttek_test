﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class PPI_TBL_LIDER_PDS
    {
        [Key]
        public long ID_LIDER_PDS { get; set; }

        [MaxLength(255)]
        public string NOMBRE { get; set; }
        public int ESTADO { get; set; }

    }
}