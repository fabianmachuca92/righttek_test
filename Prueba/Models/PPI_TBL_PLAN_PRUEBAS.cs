﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class PPI_TBL_PLAN_PRUEBAS
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, DisplayName("Id Pruebas")]
        public long ID_PRUEBAS { get; set; }
        [DisplayName("Código Proyecto")]
        public long CODIGO_PROYECTO { get; set; }

        [Required]
        [ForeignKey("PPI_TBL_LIDER_PDS_PROYECTO")]
        [DisplayName("Lider PDS de Proyecto")]
        public long LIDER_PDS_PROYECTO { get; set; }

        [Required]
        [ForeignKey("PPI_TBL_LIDER_PDS_RESPONSABLE")]
        [DisplayName("Lider PDS de Responsable")]
        public long LIDER_PDS_RESPONSABLE { get; set; }

        [DisplayName("Fecha Creacion")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime FECHA_CREACION { get; set; }

        [DisplayName("Fecha Evaluación")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime FECHA_EVALUACION { get; set; }

        [DisplayName("Fecha Modificación")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime FECHA_MODIFICACION { get; set; }

        [Required, MaxLength(255)]
        [DisplayName("Descripción")]
        public string DESCRIPCION { get; set; }

        [Required, DisplayName("Datos Prueba")]
        public string DATOS_PRUEBA { get; set; }

        [Required, DisplayName("Conclusiones")]
        public string CONCLUSIONES { get; set; }

        [Required, DisplayName("Modulo")]
        public string MODULO { get; set; }

        [Required, DisplayName("Creado Por")]
        public long CREADO_POR { get; set; }

        [Required, DisplayName("Estado")]
        public int ESTADO { get; set; }

        [ ForeignKey("Lider PDS de Proyecto")]
        public virtual PPI_TBL_LIDER_PDS PPI_TBL_LIDER_PDS_PROYECTO { get; set; }
        [ ForeignKey("Lider PDS de Responsable")]
        public virtual PPI_TBL_LIDER_PDS PPI_TBL_LIDER_PDS_RESPONSABLE { get; set; }

    }
}